<?php namespace App\Detection;

use Imagick;
use Zxing\BinaryBitmap;
use Zxing\Common\HybridBinarizer;
use Zxing\IMagickLuminanceSource;
use Zxing\NotFoundException;
use Zxing\Qrcode\Decoder\Decoder;
use Zxing\Qrcode\Detector\FinderPatternFinder;

class DetectQR
{

    public function __construct()
    {
        $this->decoder =  new Decoder();
    }

    public function getPoints($image_src) {

        $im = new Imagick();
        $im->readImage($image_src);

        $width = $im->getImageWidth();
        $height = $im->getImageHeight();

        //$im->separateImageChannel(1);
        //$im->blackThresholdImage("#999999");


        $source = new IMagickLuminanceSource($im, $width, $height);
        $histo = new HybridBinarizer($source);
        $bitmap = new BinaryBitmap($histo);

        $detector = new Detector($bitmap->getBlackMatrix());

        $finder = new FinderPatternFinder($bitmap->getBlackMatrix(), null);
        $points = $finder->find(null);

        $moduleSize = (float) $detector->calculateModuleSize($points->getTopLeft(), $points->getTopRight(), $points->getBottomLeft());
        if ($moduleSize < 1.0) {
            throw NotFoundException::getNotFoundInstance();
        }

        return array(
            'top_right' => [
                'x' => intval($points->getTopRight()->getX()),
                'y' => intval($points->getTopRight()->getY())
            ],
            'top_left' => [
                'x' => intval($points->getTopLeft()->getX()),
                'y' => intval($points->getTopLeft()->getY())
            ],
            'bottom_left' => [
                'x' => intval($points->getBottomLeft()->getX()),
                'y' => intval($points->getBottomLeft()->getY())
            ],
            'bottom_right' => [
                'x' => intval($points->getBottomRight()->getX()),
                'y' => intval($points->getBottomRight()->getY())
            ]
        );

    }


}