<?php namespace App\Transform;

class Perspective
{

    public function correctPerspective($src_image,$points,$dest_path) {

        $path_parts = pathinfo($src_image);
        $filename = $path_parts['filename'];

        $dest_file = $dest_path . '/' . $filename . '_transformed.png';
        $dest_file_white = $dest_path . '/' . $filename . '_transformed_white.png';

        $points_image[0] = "0,0";
        $points_image[1] = "0,1024";
        $points_image[2] = "1024,1024";
        $points_image[3] = "1024,0";

        $dest_points[0] = $points['top_left']['x'] . ',' . $points['top_left']['y'];
        $dest_points[1] = $points['top_right']['x'] . ',' . $points['top_right']['y'];
        $dest_points[2] = $points['bottom_right']['x'] . ',' . $points['bottom_right']['y'];
        $dest_points[3] = $points['bottom_left']['x'] . ',' . $points['bottom_left']['y'];

        $transform = "";
        for($i=0;$i<4;$i++) {
            $transform  .="   " . $dest_points[$i] . " " . $points_image[$i];
        }

        exec("convert {$src_image} -distort Perspective '$transform' -crop 844x844+90+90  {$dest_file}");
        exec("./whitebalance 0,0 {$dest_file}  {$dest_file_white}");

    }

}